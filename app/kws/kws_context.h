#ifndef APP_KWS_KWS_CONTEXT_H_
#define APP_KWS_KWS_CONTEXT_H_

#include <map>
#include <set>
#include <vector>

#include "grape/app/i_ud_context.h"

using grape::IUDContext;
using grape::vid_t;

class KWSContext : public IUDContext {
 public:
  std::map<int, std::vector<vid_t>> tag2id;
  std::unordered_map<vid_t, std::unordered_map<int, double>> dist; // vid -> keyword -> dist
  std::map<vid_t, bool> active;
  std::unordered_set<vid_t> frontier;
  std::unordered_map<vid_t, double> ans_score;
  std::priority_queue<std::pair<double, vid_t>> ans_k;
};

#endif  // APP_KWS_KWS_CONTEXT_H_
