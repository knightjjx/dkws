#include "app/kws/kws.h"

#include <fstream>
#include <iostream>
#include <limits>
#include <queue>
#include <string>
#include <unordered_set>
#include <utility>
#include <vector>

#include "glog/logging.h"

#include "grape/flags.h"
#include "grape/fragment/i_fragment.h"
#include "grape/fragment/immutable_edgecut_fragment.h"
#include "grape/message/i_message.h"
#include "grape/message/vid_double_pair_msg.h"
#include "grape/pod_any.h"
#include "grape/util.h"
#include "grape/fragment/graph.h"
#include "app/kws/kws_context.h"
#include "grape/message/i_message.h"
#include "grape/message/vid_map_msg.h"

namespace grape {

    int max = 3;
    unsigned topk = 1;

// TODO: make it more clear
// TODO: MARCO for reinterpret<T>?
    bool insert_ans(std::priority_queue<std::pair<double, vid_t>> &ans_k, unsigned topk, std::unordered_map<vid_t, std::unordered_map<int, double>>& dist, vid_t v) {

//        std::cout << "Insert an answer\t Total Size\t" << ans_k.size() << std::endl;
        double score = 0;
        for (auto d: dist[v]){
            if(d.second == std::numeric_limits<double>::max()) return false;

//            std::cout << "\tdcheck\t" << d << std::endl;
            score += d.second;
        }
//        std::cout << "Insert an answer\t Total Size\t" << ans_k.size() << std::endl;
        if (ans_k.size() < topk){
            ans_k.push(std::make_pair(score, v));
//            std::cout << "Insert an answer\t Total Size\t" << ans_k.size() << std::endl;
            return true;
        }
        if (score > ans_k.top().first) return false;
        else
            ans_k.push(std::make_pair(score, v));

        if (ans_k.size() > topk) {
            ans_k.pop();
        }
//        std::cout << "Insert an answer\t Total Size\t" << ans_k.size() << std::endl;
        return true;
    }
    void KWS::Init(unique_ptr<IFragment> &fragment, result_frag_t &partial_result,
                   shared_ptr<IUDContext> &context_ptr, const Vector<Any> &query) {
      ImmutableEdgecutFragment *frag =
              dynamic_cast<ImmutableEdgecutFragment *>(fragment.get());
//      frag->SetIntermediateData(std::to_string(0), Any(-1));
      context_ptr = shared_ptr<IUDContext>(new KWSContext());
      KWSContext *kc = dynamic_cast<KWSContext *>(context_ptr.get());
      auto &tag2id = kc->tag2id;
      auto &dist = kc->dist;

      Graph pattern(query[0].AsGraph());
      Vector<int> keywords;
      for (vid_t u = 0; u < pattern.GetVerticesNum(); u++) {
        int tag = pattern.GetVertexData(u).ToInt32();
        if (u == 0 || u == 1) continue;
        keywords.push_back(tag);
        tag2id.insert(std::make_pair(tag, std::vector<vid_t>()));
      }

      if (fragment->fid() == 0) VLOG(1) << "query keywords= " << keywords.size();

      vid_t ivnum = frag->GetInnerVerticesNum();
      vid_t ovnum = frag->GetOuterVerticesNum();
      for (vid_t v = 0; v < ivnum + ovnum; v++) {
        int tag = frag->GetData(v).ToInt32();
        for (auto key: keywords)
            dist[v].insert(std::make_pair(key, std::numeric_limits<double>::max()));
        if (tag2id.find(tag) != tag2id.end()) {
          tag2id[tag].push_back(v);
        }
      }


      vid_t gvnum = frag->GetOuterVerticesNum() + ivnum;
      for (vid_t v = 0; v < gvnum; v++) {
        int tag = frag->GetData(v).ToInt32();
        if (tag2id.find(tag) != tag2id.end()) {
          tag2id[tag].push_back(v);
        }
      }
    }
    void KWS::PINE_PEval(unique_ptr<IFragment> &fragment, MessageBuffer &messages,
                    result_frag_t &partial_result,
                    shared_ptr<IUDContext> &context_ptr,
                    const Vector<Any> &query) {

    }
    void KWS::PEval(unique_ptr<IFragment> &fragment, MessageBuffer &messages,
                    result_frag_t &partial_result,
                    shared_ptr<IUDContext> &context_ptr,
                    const Vector<Any> &query) {
//      int max = 4;
      ImmutableEdgecutFragment *frag =
              dynamic_cast<ImmutableEdgecutFragment *>(fragment.get());

      KWSContext *kc = dynamic_cast<KWSContext *>(context_ptr.get());
      auto &tag2id = kc->tag2id;
      auto &dist = kc->dist;
      auto &active = kc->active;
      auto &ans_k = kc->ans_k;
      active = std::map<vid_t, bool>();


      Graph pattern(query[0].AsGraph());
      Vector<int> keywords;
      for (vid_t u = 0; u < pattern.GetVerticesNum(); u++) {
          int tag = pattern.GetVertexData(u).ToInt32();

          if (u == 0) {
              topk = tag;
              continue;
          }
          if (u == 1){
              max = tag;
              continue;
          }
        keywords.push_back(tag);
      }

      if (fragment->fid() == 0) VLOG(1) << "query keywords = " << keywords.size();

      std::map<int, std::priority_queue<std::pair<double, vid_t>>> heaps;
      std::map<int, std::vector<bool>> visiteds;


      vid_t ivnum = frag->GetInnerVerticesNum();
      vid_t ovnum = frag->GetOuterVerticesNum();

      // Init the priority queues
      for(auto k: keywords){
//      for (unsigned i = 0; i < keywords.size(); i++) {
//        int k = keywords[i];
        auto vids = tag2id[k];
        heaps[k] = std::priority_queue<std::pair<double, vid_t>>();
        visiteds[k] = std::vector<bool>(ivnum + ovnum, false);
        for (auto vid: vids) {
          dist[vid][k] = 0;
          heaps[k].push(std::make_pair(0, vid));
          insert_ans(ans_k, topk, dist, vid);
          active[vid] = true;
//          visiteds[k][vid] = true;
        }
      }

      // Define the messages below
      std::vector<double> msg_to_send(ovnum, std::numeric_limits<double>::max());


      for(auto k: keywords){
        auto heap = heaps[k];

        while (!heap.empty()) {
          vid_t u = heap.top().second;
          double d = -heap.top().first;
          heap.pop();
          if (d > max)
              break;

          auto es = frag->_GetIncomingEdges(u);
          for (auto &iter : es) {
            vid_t v = iter.src();
            double len = frag->GetData(iter).AsDouble();
            if (dist[v][k] > d + len) {
              dist[v][k] = d + len;
              heap.push(std::make_pair(-dist[v][k], v));
              insert_ans(ans_k, topk, dist, v);
              active[v] = true;
            }
          }
        }
      }
      // Init messages
      for (vid_t v = ivnum; v < ivnum + ovnum; v++) {
        vid_t gvid = frag->Lid2Gid(v);
        if (active.find(v) != active.end()) {
          int dfid = fragment->GetFragId(gvid);
          messages.SyncStateOnOuterVertex(dfid, VidMapMsg(gvid, dist[v]));
        }
      }
      frag->SetIntermediateData(std::to_string(0), Any(-1));
    }
    void KWS::PINE_IncEval(unique_ptr<IFragment> &fragment, MessageBuffer &messages,
                      result_frag_t &partial_result,
                      shared_ptr<IUDContext> &context_ptr,
                      const Vector<Any> &query) {

    }
    void KWS::IncEval(unique_ptr<IFragment> &fragment, MessageBuffer &messages,
                      result_frag_t &partial_result,
                      shared_ptr<IUDContext> &context_ptr,
                      const Vector<Any> &query) {

      ImmutableEdgecutFragment *frag =
              dynamic_cast<ImmutableEdgecutFragment *>(fragment.get());

      KWSContext *kc = dynamic_cast<KWSContext *>(context_ptr.get());
      auto &tag2id = kc->tag2id;
      auto &dist = kc->dist;
      auto &active = kc->active;
      auto &ans_k = kc->ans_k;
      active = std::map<vid_t, bool>();
      int max = 3;
      unsigned topk = 1;
      Graph pattern(query[0].AsGraph());
      Vector<int> keywords;
      for (vid_t u = 0; u < pattern.GetVerticesNum(); u++) {
          int tag = pattern.GetVertexData(u).ToInt32();

          if (u == 0) {
              topk = tag;
              continue;
          }
          if (u == 1){
              max = tag;
              continue;
          }
        keywords.push_back(tag);
      }
      std::cout << "topk\t" << topk << std::endl;

      vid_t ivnum = frag->GetInnerVerticesNum();
      vid_t ovnum = frag->GetOuterVerticesNum();

      std::map<int, std::priority_queue<std::pair<double, vid_t>>> heaps;
      std::map<int, std::vector<bool>> visiteds;

      // Init the priority queues
      for(auto k: keywords){
//      for (unsigned i = 0; i < keywords.size(); i++) {
//        int k = keywords[i];
        auto vids = tag2id[k];
        heaps[k] = std::priority_queue<std::pair<double, vid_t>>();
        visiteds[k] = std::vector<bool>(ivnum + ovnum, false);
      }

      std::unordered_set<unsigned> updated;

      VidMapMsg msg;

      while (messages.GetMessage(msg)) {
        vid_t u = fragment->Gid2Lid(msg.vid());
        auto dis = msg.value();
        for(auto k: keywords){
//        for (unsigned i = 0; i < keywords.size(); i++) {
          if (dist[u][k] > dis[k]) {
            dist[u][k] = dis[k];
            heaps[k].push(std::make_pair(-dist[u][k], u));
            insert_ans(ans_k, topk, dist, u);
//            visiteds[keywords[i]][u] = true;
            active[u] = true;
          }
        }
      }

      // Define the messages below
      std::vector<double> msg_to_send(ovnum, std::numeric_limits<double>::max());

      for(auto k: keywords){
//      for (unsigned i = 0; i < keywords.size(); i++) {
//        int k = keywords[i];
        auto heap = heaps[k];
        while (!heap.empty()) {
          vid_t u = heap.top().second;
          double d = -heap.top().first;
          heap.pop();

          if (d > max)
            break;

          auto es = frag->_GetIncomingEdges(u);
          for (auto &iter : es) {
            vid_t v = iter.src();
            double len = frag->GetData(iter).AsDouble();
            if (dist[v][k] > d + len) {
              dist[v][k] = d + len;
              heap.push(std::make_pair(-dist[v][k], v));
              insert_ans(ans_k, topk, dist, v);
              active[v] = true;
            }
          }
        }
      }
      if (fragment->fid() == 1) VLOG(1) << "Size of active\t " << active.size();
      // Init messages
      for (vid_t v = ivnum; v < ivnum+ovnum; v++) {
        vid_t gvid = frag->Lid2Gid(v);
        if (active.find(v) != active.end()) {
          int dfid = fragment->GetFragId(gvid);
          messages.SyncStateOnOuterVertex(dfid, VidMapMsg(gvid, dist[v]));
        }
      }
//      frag->SetIntermediateData(std::to_string(0), Any(-1));
    }

    void KWS::ProcessMsgInCoordinator(const Vector<Vector<Map<String, Any>>> &messages,
                                         const Vector<Any> &query, Map<String, Any> &message_to_worker,
                                         bool &is_terminate) {

        message_to_worker[std::to_string(1)] = Any(0);
        message_to_worker[std::to_string(0)] = Any(-1);

//        message_to_worker[std::to_string(0)] = Any(-1);
//        is_terminate = true;
    }

    void KWS::Assemble(result_frag_t &partial_result, result_frag_t &result,
                       const Vector<Any> &query) {
    }

    void KWS::WriteToFile(unique_ptr<IFragment> &fragment,
                          result_frag_t &partial_result,
                          shared_ptr<IUDContext> &context_ptr,
                          const String &prefix, const Vector<Any> &query) {
    }
}  // namespace grape
