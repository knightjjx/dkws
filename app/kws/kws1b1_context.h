#ifndef APP_KWS_KWS_CONTEXT_H_
#define APP_KWS_KWS_CONTEXT_H_

#include <map>
#include <set>
#include <vector>

#include "grape/app/i_ud_context.h"

#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/page_rank.hpp>
using namespace boost;

struct pads {
    // distance map: (w,d)
    std::unordered_map<int, int> pads;

    int vid;
};

struct EdgeWeight {
    double weight;
};

typedef adjacency_list<vecS, vecS, directedS, pads, property<edge_weight_t, EdgeWeight>> PGraph;
typedef property_map<PGraph, vertex_index_t>::type IndexMap;
typedef iterator_property_map<std::vector<double>::iterator, IndexMap> RankMap;
typedef graph_traits<PGraph>::vertex_iterator VIter;
typedef graph_traits<PGraph>::out_edge_iterator EIter;
typedef graph_traits<PGraph>::vertex_descriptor vertex_descriptor;
typedef graph_traits<PGraph>::edge_descriptor edge_descriptor;
typedef std::pair<int, int> Edge;
typedef graph_traits<PGraph>::vertex_descriptor VD;


using grape::IUDContext;
using grape::vid_t;

class KWSContext : public IUDContext {
 public:
  std::map<int, std::vector<vid_t>> tag2id;
  std::unordered_map<vid_t, std::unordered_map<int, double>> dist; // vid -> keyword -> dist
  std::unordered_map<vid_t, std::unordered_map<int, double>> fd; //forward distance: vid -> keyword -> dist
  std::unordered_map<vid_t, bool> active;
  std::map<int, std::unordered_set<vid_t>> frontier;
  int CurKeywordIndex;
  std::unordered_map<vid_t, double> ans_score;
  std::priority_queue<std::pair<double, vid_t>> ans_k;
  std::map<vid_t, std::unordered_map<unsigned, std::unordered_map<int, double>>> cMsg;
  std::unordered_set<vid_t> to_forward;
  std::unordered_set<vid_t> visited_forward;

  vid_t flag_id;

  /** Accumulated messages for three types */
  std::unordered_map<vid_t, std::unordered_map<int, double>> b_msg;
  std::unordered_map<vid_t, std::unordered_map<int, double>> f_msg;
  std::map<vid_t, std::unordered_map<int, double>> invert_msg;
  std::map<int, std::priority_queue<std::pair<double, vid_t>>> heaps;

};

#endif  // APP_KWS_KWS_CONTEXT_H_
