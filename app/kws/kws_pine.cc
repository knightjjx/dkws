#include "app/kws/kws_pine.h"

#include <fstream>
#include <iostream>
#include <sstream>
#include <limits>
#include <queue>
#include <string>
#include <unordered_set>
#include <utility>
#include <vector>

#include "glog/logging.h"

#include "grape/flags.h"
#include "grape/fragment/i_fragment.h"
#include "grape/fragment/immutable_edgecut_fragment.h"
#include "grape/message/i_message.h"
#include "grape/message/vid_double_pair_msg.h"
#include "grape/pod_any.h"
#include "grape/util.h"
#include "grape/fragment/graph.h"
#include "app/kws/kws1b1_context.h"
#include "grape/message/i_message.h"
#include "grape/message/vid_map_double_pair_msg.h"

#define BFirst_MODE 0
#define FFirst_MODE 1
#define INC_MSG_MODE 2
#define DES_MSG_MODE 3
#define Random_MODE 4
#define COST_MODE 5
#define COST_REVERSE_MODE 6

int k_pads = 1;
bool np = true;
bool enable_pads = false;
bool enable_pine = true;
int fb_mode_ = 0;
int total = 16;
int max = 3; // tau
unsigned topk = 1;



void load_config(){
    std::ifstream myfile("kws.config");
    myfile >> np >> enable_pads >> enable_pine >> fb_mode_ >> max >> topk;
    std::cout << "np\t" << np << "\tenable_pads\t" << enable_pads << "\tenable_pine\t" << enable_pine << "\tfb_mode\t" << fb_mode_ << "\ttau\t" << max << "\ttopk\t" << topk << std::endl;
}


void merge(std::unordered_map<int, double> &total, std::unordered_map<int, double> &label) {
    for (auto center: label) {
        if (total.find(center.first) == total.end()) {
            total.insert(center);
        } else {
            total[center.first] = std::min(total[center.first], center.second);
        }
    }
}

bool pads_skip(std::unordered_map<int, double> &l1, std::unordered_map<int, double> &l2, std::unordered_map<int, double> &l3,
               double budget) {
    /*
    if (l1.size() > 10)
        VLOG(1) << "Size of label\t" << l1.size() << "\t" << l2.size() << "\t" << l3.size();
    if (l1.size() > 50)
        return false;
        */
    bool tokeywords = true;
    bool toportals = true;
    for (auto center: l1) {
        if (l2.find(center.first) == l2.end())
            continue;
        double bound = center.second - l2[center.first];

        if (bound >= budget) {
            tokeywords = false;
            break;
        }
    }

    for (auto center: l1) {
        if(l3.find(center.first) == l3.end())
            continue;
        double bound = center.second - l3[center.first];

        if (bound >= budget) {
            toportals = false;
            break;
        }
    }
    if (!tokeywords && !toportals)
        return true;
    return false;
}

std::vector<double> prvalue(PGraph g) {
    std::vector<double> ranks(num_vertices(g));
    page_rank(g, make_iterator_property_map(ranks.begin(), get(vertex_index, g)),
              graph::n_iterations(1000), 0.85, num_vertices(g));


    return ranks;
}

std::vector<double> adsrandomevalue(PGraph g) {
    std::vector<double> ranks(num_vertices(g));
    for (unsigned i = 0; i < ranks.size(); ++i) {
        ranks[i] = ((double) rand() / (RAND_MAX));
    }
    return ranks;
}

bool IScenter(double dist, int v, std::unordered_map<int, std::unordered_map<int, double>>& ads_labels) {
    auto ads = ads_labels[v];
    int cnt = 0;
    for (auto e: ads) {
        if (e.second <= dist)
            cnt++;
        if (cnt >= k_pads)
            return false;
    }
    return true;
}

void prune_dijkstra(PGraph &g, int source, std::unordered_map<int, VD>& nodes, std::unordered_map<int, std::unordered_map<int, double>>& ads_labels) {
    std::priority_queue<std::pair<double, int>> heap;
    std::unordered_set<int> visited;
    std::vector<double> dist(num_vertices(g));

    for (unsigned i = 0; i < dist.size(); i++) {
        dist[i] = std::numeric_limits<double>::max();
    }

    dist[source] = 0;
    heap.push(std::make_pair(0, source));

    while (!heap.empty()) {
        int u = heap.top().second;
        double dis = -heap.top().first;
        heap.pop();

        if (visited.find(u) != visited.end()) {
            continue;
        }

        if (IScenter(dis, u, ads_labels)) {
            ads_labels[u].insert(std::make_pair(source,dis));
//            ads_labels[u][source] = dis;
        } else {
            visited.insert(u);
            continue;
        }

        visited.insert(u);

        EIter i, end;

        for (tie(i, end) = out_edges(nodes[u], g); i != end; i++) {
            auto v = target(*i, g);
            auto len = get(edge_weight_t(), g, *i).weight;
            if (dist[v] > dist[u] + len) {
                dist[v] = dist[u] + len;
                heap.push(std::make_pair(-dist[v], v));
            }
        }
    }

    return;
}

void prads(PGraph &g, std::vector<double> ranks, int fid, std::unordered_map<int, std::unordered_map<int, double>>& ads_labels, std::unordered_map<int, VD>& nodes) {
    std::map<int, double> rmap;
    for (unsigned i = 0; i < ranks.size(); ++i) {
        rmap[i] = ranks[i];
        std::unordered_map<int, double> tmp;
        ads_labels[i] = tmp;
    }

//    VLOG(1) << "fid\t" << fid << "\tCheck 2";

    // Declaring the type of Predicate that accepts 2 pairs and return a bool
    typedef std::function<bool(std::pair<int, double>, std::pair<int, double>)> Comparator;

    Comparator compFunctor =
            [](std::pair<int, double> elem1, std::pair<int, double> elem2) {
                return elem1.second <= elem2.second;
            };
    std::set<std::pair<int, double>, Comparator> decending_order_rank(
            rmap.begin(), rmap.end(), compFunctor);
//    int cnt = 0;

    for (auto element : decending_order_rank) {
//        if (cnt++ % 1000 == 0) {
//            std::cout << "[Finish]\t" << cnt << std::endl;
//        }
        prune_dijkstra(g, element.first, nodes, ads_labels);

        /*
        if(fid == 0)
        VLOG(1) << "VID\t" << element.first;

        if(fid == 0)
        for(auto e: ads_labels[element.first]){
          VLOG(1) << e.first << "\t" << e.second;
        }
         */

    }
//    VLOG(1) << "fid\t" << fid << "\tCheck 3";

    return;
}



namespace grape {

// TODO: make it more clear
// TODO: MARCO for reinterpret<T>?
    bool FPrint(unsigned fid){
        if (fid == 1)
            return true;
        return false;
    }
    /**
    void print_label(unique_ptr<IFragment> &fragment){
        ImmutableEdgecutFragment *frag =
                dynamic_cast<ImmutableEdgecutFragment *>(fragment.get());

    }
     */

    PGraph loadGraph(unique_ptr<IFragment> &fragment, std::unordered_map<int, VD>& nodes, bool in) {
        PGraph g;
        ImmutableEdgecutFragment *frag =
                dynamic_cast<ImmutableEdgecutFragment *>(fragment.get());
        /**
         * Load Vertices
         */
        vid_t ivnum = frag->GetInnerVerticesNum();
        vid_t gvnum = frag->GetOuterVerticesNum() + ivnum;

        for (vid_t v = 0; v < gvnum; v++) {
            nodes[v] = add_vertex(g);
        }


        if(FPrint(frag->fid())) std::cout << "Finish loading vertices\t" << nodes.size() << "\tivnum\t" << ivnum << "\tgvnum\t" << gvnum<< "\t" << num_vertices(g) << std::endl;
        /*
        if(FPrint(frag->fid())) {
            for (vid_t v = ivnum; v < gvnum; v++) {
                std::cout << "v_lid\t" << v << "\tv_oid\t" << frag->Gid2Oid(frag->Lid2Gid(v)) << "\tis border\t"  << std::endl;
            }
        }
         */
        /**
         * Load Edges
         */
//        wedge.weight = 1;


        for (vid_t v = 0; v < gvnum; v++) {
            auto et = frag->_GetOutgoingEdges(v);
//            if(FPrint(frag->fid())) std::cout << "source vertex\t" << v << "\tori\t" << frag->Gid2Oid(frag->Lid2Gid(v))<< "\t" << "Out border vertex\t" << frag->IsOutgoingBorderVertex(v) << std::endl;
            for (auto &iter : et) {
                EdgeWeight wedge;

                wedge.weight = frag->GetData(iter).AsDouble();
                vid_t v_dst = iter.dst();

//                if(FPrint(frag->fid())) std::cout << "target vertex\t" << v_dst << "\tori\t" << frag->Gid2Oid(frag->Lid2Gid(v_dst))<< "\t";

                if (in)
                    add_edge(nodes[v], nodes[v_dst], wedge, g);
                else
                    add_edge(nodes[v_dst], nodes[v], wedge, g);
//                add_edge(nodes[v_dst], nodes[v], wedge, g);

            }
//            if(FPrint(frag->fid())) std::cout << std::endl;
        }

        if(FPrint(frag->fid())) std::cout << num_edges(g) << std::endl;
        if(FPrint(frag->fid())) std::cout << "Finish loading edges" << std::endl;

        return g;
    }

    void build_bpads(unique_ptr<IFragment> &fragment, std::unordered_map<int, std::unordered_map<int, double>>& ads_labels, std::unordered_map<int, double>& bads_labels) {
        ImmutableEdgecutFragment *frag = dynamic_cast<ImmutableEdgecutFragment *>(fragment.get());

        vid_t ivnum = frag->GetInnerVerticesNum();
        vid_t gvnum = frag->GetOuterVerticesNum() + ivnum;
        for (vid_t v = ivnum; v < gvnum; v++) {
//            if (frag->IsOutgoingBorderVertex(v)) {
                merge(bads_labels, ads_labels[v]);
//            }
        }
        VLOG(1) << "Tell me the size of bpads\t" << bads_labels.size();
    }

    void build_kads(unique_ptr<IFragment> &fragment, std::unordered_map<int, std::unordered_map<int, double>>& ads_labels, std::unordered_map<int, std::unordered_map<int, double>>& kads_labels) {
        ImmutableEdgecutFragment *frag = dynamic_cast<ImmutableEdgecutFragment *>(fragment.get());

        vid_t ivnum = frag->GetInnerVerticesNum();
//        vid_t gvnum = frag->GetOuterVerticesNum() + ivnum;
        for (vid_t v = 0; v < ivnum; v++) {
            int tag = frag->GetData(v).ToInt32();
            if (kads_labels.find(tag) == kads_labels.end()) {
                std::unordered_map<int, double> tmp;
                kads_labels.insert(std::make_pair(tag, tmp));
            }
            merge(kads_labels[tag], ads_labels[v]);
        }
    }

    double insert_ans(std::priority_queue<std::pair<double, vid_t>> &ans_k, unsigned topk, double score, vid_t v) {
        ans_k.push(std::make_pair(score, v));
//        std::cout << "Insert an answer\t Total Size\t" << ans_k.size() << std::endl;
        if (ans_k.size() > topk) {
            ans_k.pop();
            return ans_k.top().first;
        } else if (ans_k.size() == topk) {
            return ans_k.top().first;
        } else {
            return std::numeric_limits<double>::max();
        }

    }

    int select_min_head(std::unordered_map<int, double>& heads, std::unordered_map<int, int>& cnts){
        int k = -1;
        double min_head = std::numeric_limits<double>::max();
        for (auto h: heads) {
            if (min_head > h.second) {
                min_head = h.second;
                k = h.first;
            } else if (min_head == h.second && cnts[k] > cnts[h.first]) {
                k = h.first;
            }
        }
        return k;
    }


    bool select_bkws_fkws(shared_ptr<IUDContext> &context_ptr, bool reverse = false){
        KWSContext *kc = dynamic_cast<KWSContext *>(context_ptr.get());

        /** Accumulate the messages */
//        auto &fd = kc->fd;
        auto &b_msg = kc->b_msg;
        auto &f_msg = kc->f_msg;
        std::cout << "selecting BKWS and FKWS\t" << "# of b_msg\t" << b_msg.size() << "\t# of f_msg\t" << f_msg.size() << std::endl;

        if (!reverse) {
            if (b_msg.size() > f_msg.size())
                return true;
            else
                return false;
        } else {
            if (b_msg.size() > f_msg.size())
                return true;
            else
                return false;
        }
    }


    bool select_bkws_fkws_by_cost(shared_ptr<IUDContext> &context_ptr, bool reverse = false){
        KWSContext *kc = dynamic_cast<KWSContext *>(context_ptr.get());

        /** Accumulate the messages */
//        auto &fd = kc->fd;
        auto &b_msg = kc->b_msg;
        auto &f_msg = kc->f_msg;
        double b_cost = 0, f_cost = 0;
        for (auto &m: b_msg){
            for(auto kv: m.second){
                b_cost += kv.second;
            }
        }
        for(auto &m: f_msg){
            for(auto kv: m.second){
                f_cost += kv.second;
            }
        }
        std::cout << "selecting BKWS and FKWS\t" << "cost of b_msg\t" << b_cost << "\tcost of f_msg\t" << f_cost << std::endl;

        if (!reverse) {
            if (b_cost > f_cost)
                return true;
            else
                return false;
        } else {
            if (b_cost > f_cost)
                return true;
            else
                return false;
        }
    }


    bool select_bkws_fkws_random(shared_ptr<IUDContext> &context_ptr){
        KWSContext *kc = dynamic_cast<KWSContext *>(context_ptr.get());

        /** Accumulate the messages */
//        auto &fd = kc->fd;
        auto &b_msg = kc->b_msg;
        auto &f_msg = kc->f_msg;
        std::cout << "selecting BKWS and FKWS\t" << "# of b_msg\t" << b_msg.size() << "\t# of f_msg\t" << f_msg.size() << std::endl;

        if (b_msg.size() == 0)
            return false;
        if (f_msg.size() == 0)
            return true;
        srand(time(0));
        return rand() % 2 == 0;
    }

    bool select_bkws_fkws_bfirst(shared_ptr<IUDContext> &context_ptr){
        KWSContext *kc = dynamic_cast<KWSContext *>(context_ptr.get());

        /** Accumulate the messages */
        VidMapDoublePairMsg msg;
//        auto &fd = kc->fd;
        auto &b_msg = kc->b_msg;
        auto &f_msg = kc->f_msg;
        std::cout << "selecting BKWS and FKWS\t" << "# of b_msg\t" << b_msg.size() << "\t# of f_msg\t" << f_msg.size() << std::endl;

        if(b_msg.size() != 0)
            return true;
        else
            return false;
    }

    bool select_bkws_fkws_ffirst(shared_ptr<IUDContext> &context_ptr){
        KWSContext *kc = dynamic_cast<KWSContext *>(context_ptr.get());

        /** Accumulate the messages */
        VidMapDoublePairMsg msg;
//        auto &fd = kc->fd;
        auto &b_msg = kc->b_msg;
        auto &f_msg = kc->f_msg;
        std::cout << "selecting BKWS and FKWS\t" << "# of b_msg\t" << b_msg.size() << "\t# of f_msg\t" << f_msg.size() << std::endl;

        if(f_msg.size() != 0)
            return false;
        else
            return true;
    }

    void KWSPINE::Init(unique_ptr<IFragment> &fragment, result_frag_t &partial_result,
                      shared_ptr<IUDContext> &context_ptr, const Vector<Any> &query) {
        ImmutableEdgecutFragment *frag =
                dynamic_cast<ImmutableEdgecutFragment *>(fragment.get());

        context_ptr = shared_ptr<IUDContext>(new KWSContext());
        KWSContext *kc = dynamic_cast<KWSContext *>(context_ptr.get());
        auto &tag2id = kc->tag2id;
        auto &dist = kc->dist;
        load_config();
//        auto &fd = kc->fd;

        if (np)
            frag->notify(frag->fid(), std::numeric_limits<double>::max());

        /** Build the page-rank index */
        if (enable_pads) {
            if (!frag->index) {
                /** in labels */
                std::unordered_map<int, VD> nodes;
                PGraph g = loadGraph(fragment, nodes, true);
                std::vector<double> rankvalue;
                rankvalue = prvalue(g);
                prads(g, rankvalue, frag->fid(), frag->in_ads_labels, nodes);
                build_bpads(fragment, frag->in_ads_labels, frag->in_bads_labels);
                build_kads(fragment, frag->in_ads_labels, frag->in_kads_labels);

                /** out labels */
                std::unordered_map<int, VD> nodes_out;
                g = loadGraph(fragment, nodes_out, false);
                rankvalue = prvalue(g);
                prads(g, rankvalue, frag->fid(), frag->out_ads_labels, nodes_out);
                build_bpads(fragment, frag->out_ads_labels, frag->out_bads_labels);
                build_kads(fragment, frag->out_ads_labels, frag->out_kads_labels);

                frag->index = true;
            }
        }

        Graph pattern(query[0].AsGraph());
        Vector<int> keywords;

        for (vid_t u = 0; u < pattern.GetVerticesNum(); u++) {
            int tag = pattern.GetVertexData(u).ToInt32();

            keywords.push_back(tag);
            tag2id.insert(std::make_pair(tag, std::vector<vid_t>()));
        }


        if (fragment->fid() == 1) VLOG(1) << "query keywords= " << keywords.size();

        vid_t ivnum = frag->GetInnerVerticesNum();
        vid_t ovnum = frag->GetOuterVerticesNum();
        for (vid_t v = 0; v < ivnum + ovnum; v++) {
            int tag = frag->GetData(v).ToInt32();
            dist[v] = std::unordered_map<int, double>();

/**
            if (frag->IsOutgoingBorderVertex(v)) {
                fd[v] = std::unordered_map<int, double>();
            }
*/
            if (tag2id.find(tag) != tag2id.end()) {
                tag2id[tag].push_back(v);
            }
        }

        vid_t gvnum = frag->GetOuterVerticesNum() + ivnum;
        for (vid_t v = ivnum; v < gvnum; v++) {
            int tag = frag->GetData(v).ToInt32();
            dist[v] = std::unordered_map<int, double>();

            if (tag2id.find(tag) != tag2id.end()) {
                tag2id[tag].push_back(v);
            }
        }


    }
    void KWSPINE::PINE_PEval(unique_ptr<IFragment> &fragment, MessageBuffer &messages,
                       result_frag_t &partial_result,
                       shared_ptr<IUDContext> &context_ptr,
                       const Vector<Any> &query) {
        std::cout << "******************* ON PINE MODEL -- Stage PEval *******************" << std::endl;
        PINE_PEval_BKWS(fragment, messages, partial_result, context_ptr, query);
        PINE_PEval_FKWS(fragment, messages, partial_result, context_ptr, query);
    }
    void KWSPINE::PINE_PEval_BKWS(unique_ptr<IFragment> &fragment, MessageBuffer &messages,
                             result_frag_t &partial_result,
                             shared_ptr<IUDContext> &context_ptr,
                             const Vector<Any> &query) {

        double lower_bound = std::numeric_limits<double>::max();

        ImmutableEdgecutFragment *frag =
                dynamic_cast<ImmutableEdgecutFragment *>(fragment.get());

        KWSContext *kc = dynamic_cast<KWSContext *>(context_ptr.get());
        auto &tag2id = kc->tag2id;
        auto &dist = kc->dist;
//        auto &fd = kc->fd;
        auto &active = kc->active;
        auto &ans_score = kc->ans_score;
        auto &ans_k = kc->ans_k;
        auto heaps = kc->heaps;
//        auto &visited_forward = kc->visited_forward;

        std::unordered_map<vid_t, std::unordered_map<unsigned, std::unordered_map<int, double>>> cMsg;

        active = std::unordered_map<vid_t, bool>();

        Graph pattern(query[0].AsGraph());
        Vector<int> keywords;

        for (vid_t u = 0; u < pattern.GetVerticesNum(); u++) {
            int tag = pattern.GetVertexData(u).ToInt32();
            keywords.push_back(tag);
        }

        if (fragment->fid() == 1) VLOG(1) << "query keywords = " << keywords.size();


//        std::map<int, std::priority_queue<std::pair<double, vid_t>>> heaps;
        std::unordered_map<int, std::vector<bool>> visiteds;

        vid_t ivnum = frag->GetInnerVerticesNum();
        vid_t ovnum = frag->GetOuterVerticesNum();
        if(FPrint(frag->fid())) std::cout << fragment->fid() << "\t # of edges\t" << frag->GetEdgeNum() << std::endl;
        if(FPrint(frag->fid())) std::cout << fragment->fid() << "\t # of Outer Vertex\t" << ovnum << std::endl;
        if(FPrint(frag->fid())) std::cout << fragment->fid() << "\t # of Inner Vertex\t" << ivnum << std::endl;
        std::unordered_map<int, double> heads = std::unordered_map<int, double>();
        std::unordered_map<int, int> cnts;


        if (fragment->fid() == 5) VLOG(1) << "TCheck 1";
        // Init the priority queues

        for (auto k: keywords) {
            auto vids = tag2id[k];
//            VLOG(1) << "keyword\t " << k << "\t matches\t" << vids.size();
            heaps[k] = std::priority_queue<std::pair<double, vid_t>>();
            visiteds[k] = std::vector<bool>(ivnum + ovnum, false);
            cnts[k] = 0;

            for (auto vid: vids) {
//                std::cout << "vid\t" << vid << "\tisDist\t" << (dist.find(vid) == dist.end()) << "\tk\t" << k << std::endl;
//                dist[vid][k] = 0;
                dist[vid].insert(std::make_pair(k, 0));
                ans_score[vid] = 0;
                cnts[k]++;

                auto es = frag->_GetIncomingEdges(vid);

                for (auto &iter : es) {
                    vid_t vid_parent = iter.src();
                    heaps[k].push(std::make_pair(-frag->GetData(iter).AsDouble(), vid_parent));
                    active[vid_parent] = true;
                }
                active[vid] = true;
            }
        }



        double sum_heads = 0;


        for(auto k: keywords){
            if(!heaps[k].empty())
                heads[k] = - heaps[k].top().first;
            sum_heads += heads[k];
        }


        int k = 0;
        if(!keywords.empty())
            k = keywords[0];
        else
            return;
        k = select_min_head(heads, cnts);
        if (k == -1)
            return;

        int cnt_bkws = 0;

        if(FPrint(frag->fid())) std::cout << "sum_heads\t" << sum_heads << "\tlower_bound\t" << lower_bound << std::endl;
        while (sum_heads < lower_bound) {
            cnt_bkws++;
            if (!heaps[k].empty()) {
                vid_t u = heaps[k].top().second;
                double d = -heaps[k].top().first;
//                if(FPrint(frag->fid())) std::cout << frag->fid() << "\tssworking on keyword \t " << k  << "\td\t" << d <<"\toid\t" << frag->Gid2Oid(frag->Lid2Gid(u))+1 << std::endl;
                if (d > heads[k]) {
                    sum_heads += (d - heads[k]);
                    heads[k] = d;
                }
                heaps[k].pop();
                cnts[k]++;
                visiteds[k][u] = true;

                if (d >= max) {
                    heaps.erase(k);
                }

                if (dist[u].find(k) == dist[u].end()) {
                    ans_score[u] += d;
                    dist[u].insert(std::make_pair(k, d));
                } else
                    continue;

                if (u < ivnum && dist[u].size() == keywords.size() &&
                    ((!ans_k.empty() && ans_k.top().first > ans_score[u]) || ans_k.size() < topk)) {
                    lower_bound = insert_ans(ans_k, topk, ans_score[u], u);
                    if(FPrint(frag->fid())) std::cout << frag->fid() << "\t PEval - Lower bound\t" << lower_bound << "\t" << u << std::endl;
                    if (np && lower_bound != std::numeric_limits<double>::max()) {
                        frag->notify(frag->fid(), lower_bound);
                        lower_bound = frag->push();
                    }
                }

                auto es = frag->_GetIncomingEdges(u);

                for (auto &iter : es) {
                    vid_t v = iter.src();
                    if (visiteds[k][v]) continue;
                    double len = frag->GetData(iter).AsDouble();

                    double v2k_dist = d + len;
                    if (v2k_dist > max)
                        continue;

                    heaps[k].push(std::make_pair(-v2k_dist, v));
                    active[v] = true;
                }
            } else {
                heaps.erase(k);
                heads[k] = std::numeric_limits<double>::max();
            }
            if (heaps.empty()) {
//                VLOG(1) << "I am breaking out";
                break;
            }

            k = select_min_head(heads, cnts);
            if (k == -1)
                break;

            if(np && cnt_bkws % 100000 ==0)
                lower_bound = frag->push();
        }
        if(FPrint(frag->fid())) std::cout << "sum_heads\t" << sum_heads << "\tlower_bound\t" << lower_bound << std::endl;


        /**
        vid_t gvnum = frag->GetOuterVerticesNum() + ivnum;
        for (vid_t v = 0; v < ivnum; v++){
            vid_t gvid = frag->Lid2Gid(v);
            if(FPrint(frag->fid())) std::cout << "\t BKWSovid \t" << frag->Gid2Oid(gvid) << "\t v \t" << v << "\t is in coming border vertex\t" << frag->IsIncomingBorderVertex(v) << "\t gvnum\t"  << std::endl;
        }
        for (vid_t v = 0; v < gvnum; v++){
            vid_t gvid = frag->Lid2Gid(v);
            if(FPrint(frag->fid())) std::cout << "\t BKWSovid \t" << frag->Gid2Oid(gvid) << "\t v \t" << v << "\t is out going border vertex\t" << frag->IsOutgoingBorderVertex(v) << "\t gvnum\t" << std::endl;
        }
        */

        // Init messages
        vid_t gvnum = frag->GetOuterVerticesNum() + ivnum;
        for (vid_t v = ivnum; v < gvnum; v++) {
            vid_t gvid = frag->Lid2Gid(v);
            if (active.find(v) != active.end()) {
                /** Backward search on other fratgments, MSG type is 0*/
                cMsg[v].insert(std::make_pair(0, dist[v]));
//                cMsg[v].insert(std::make_pair(2, fd[v]));
                int dfid = fragment->GetFragId(gvid);
                messages.SyncStateOnOuterVertex(dfid, VidMapDoublePairMsg(gvid, cMsg[v]));
//                if(FPrint(frag->fid())) std::cout << "MSG -- PEval -- BKWS \t" << v << std::endl;
            }
        }
        std::cout << "Lower bound on \t" << frag->fid() << "\t bound \t" << lower_bound << std::endl;
        frag->SetIntermediateData(std::to_string(0), Any(-1));
        frag->SetIntermediateData(std::to_string(2), Any(lower_bound));
    }
    void KWSPINE::PINE_PEval_FKWS(unique_ptr<IFragment> &fragment, MessageBuffer &messages,
                             result_frag_t &partial_result,
                             shared_ptr<IUDContext> &context_ptr,
                             const Vector<Any> &query) {

        /** TODO:setting NP-config*/

        double lower_bound;

        if(np) lower_bound = frag->push();
        else lower_bound = std::numeric_limits<double>::max();

        ImmutableEdgecutFragment *frag =
                dynamic_cast<ImmutableEdgecutFragment *>(fragment.get());

        KWSContext *kc = dynamic_cast<KWSContext *>(context_ptr.get());
//        auto &tag2id = kc->tag2id;
        auto &dist = kc->dist;
        auto &fd = kc->fd;
        auto &active = kc->active;
        auto &ans_score = kc->ans_score;
        auto &ans_k = kc->ans_k;
        auto &visited_forward = kc->visited_forward;
        auto &to_forward = kc->to_forward;
//        auto &heaps = kc->heaps;

        std::unordered_map<vid_t, std::unordered_map<unsigned, std::unordered_map<int, double>>> cMsg;
        vid_t ivnum = frag->GetInnerVerticesNum();
        active = std::unordered_map<vid_t, bool>();

        Graph pattern(query[0].AsGraph());
        Vector<int> keywords;

        for (vid_t u = 0; u < pattern.GetVerticesNum(); u++) {
            int tag = pattern.GetVertexData(u).ToInt32();

            keywords.push_back(tag);
        }


        if (fragment->fid() == 1) VLOG(1) << "query keywords = " << keywords.size();

        std::map<int, std::priority_queue<std::pair<double, vid_t>>> heaps;

        for (auto k: keywords) {
            heaps[k] = std::priority_queue<std::pair<double, vid_t>>();
        }

        vid_t gvnum =  frag->GetOuterVerticesNum() + ivnum;
        for (vid_t v = 0; v < ivnum; v++) {
            if (dist[v].size() == keywords.size() || dist[v].size() == 0) {
                continue;
            }
            if (lower_bound != std::numeric_limits<double>::max() && lower_bound <= ans_score[v]) {
                continue;
            }

            for (auto k: keywords) {
                if (dist[v].find(k) == dist[v].end()) {
                    to_forward.insert(v);
                    if (lower_bound != std::numeric_limits<double>::max()) {
                        if (lower_bound < ans_score[v]) continue;
                        if (lower_bound - ans_score[v] < max) {
                            heaps[k].push(std::make_pair(-(lower_bound - ans_score[v]), v));
                        }
                        else {
                            heaps[k].push(std::make_pair(-max, v));
                        }
                    } else
                        heaps[k].push(std::make_pair(-max, v));
                }
            }
        }


        /** For checking the forwarding candidates*/
        /**
        for(auto k: keywords){
            if (FPrint(frag->fid())) std::cout << "k\t" << k << std::endl;
            while(!heaps[k].empty()) {
                auto d = -heaps[k].top().first;
                vid_t v = heaps[k].top().second;
                heaps[k].pop();
                if (FPrint(frag->fid()))
                    std::cout << "Forwarding ele: vid\t" << frag->Gid2Oid(frag->Lid2Gid(v)) << "\td\t" << d << std::endl;
            }
        }
         */
        /** For checking the forwarding candidates*/


        if (FPrint(fragment->fid())) std::cout << "# for forwarding # \t" << to_forward.size() << std::endl;
        if (true) {
            if(np)
                lower_bound = fragment->push();
            // f_ans records the answers missing some keyword (f_ans = false answers)
            std::unordered_set<vid_t> f_ans;
            for (auto k: keywords) {
                int cnt_skip = 0;
                if (FPrint(fragment->fid())) std::cout << frag->fid() << "\tForwarding\tkw\t" << k << "\t#\t" << heaps[k].size() << std::endl;

                /** Those have been forwardly search */

            std::unordered_set<vid_t> k_forward_search;
                while (!heaps[k].empty()) {
                    vid_t v = heaps[k].top().second;
                    double delta_b = -heaps[k].top().first;
//                    if(FPrint(frag->fid())) std::cout << "\tforwarding on vid\t" << v << "\tk\t" << k << "\tdelta_b\t" << delta_b << "\t oid \t" << frag->Gid2Oid(frag->Lid2Gid(v)) << std::endl;
                    visited_forward.insert(v);
//                if (fragment->fid() == 1) VLOG(1) << "Forwarding\t" << v << "\t# delta_b\t" << delta_b;
                    heaps[k].pop();
                    if (f_ans.find(v) != f_ans.end()) {
//                    if (fragment->fid() == 1) VLOG(1) << "skip\t" << f_ans.size();
                        continue;
                    }
                    
                    if(k_forward_search.find(v) != k_forward_search.end())
                        continue;
                    k_forward_search.insert(v);
                    

                    /** pads_skip control */

                    if (enable_pads && pads_skip(frag->out_ads_labels[v], frag->out_kads_labels[k], frag->out_bads_labels, delta_b)) {
//                    if (fragment->fid() == 5) VLOG(1) << "Skip ....";
                        f_ans.insert(v);
                        cnt_skip++;
                        continue;
                    }
                    if(lower_bound < ans_score[v]) continue;
                    double delta_hat = lower_bound - ans_score[v];
                    if (delta_b > delta_hat) delta_b = delta_hat;


                    std::priority_queue<std::pair<double, vid_t>> v_heap = std::priority_queue<std::pair<double, vid_t>>();
                    v_heap.push(std::make_pair(0, v));

	                std::unordered_map<vid_t, vid_t> invert;

                    std::unordered_set<vid_t> v_visited;
                    while (!v_heap.empty()) {
                        vid_t v_head = v_heap.top().second;
                        v_visited.insert(v_head);
                        double v_d = -v_heap.top().first;
                        v_heap.pop();
                        if (v_d >= delta_b || (dist[v].find(k) != dist[v].end() && v_d >= dist[v][k])) {
//                            std::cout << "\tv_d\t" << v_d << "\tdelta_b\t" << delta_b << std::endl;
                            break;
                        }

                        bool hasK = (dist[v_head].find(k) != dist[v_head].end());
                        if (hasK) {

                            if (dist[v_head][k] + v_d >= delta_b) {
                                if(FPrint(frag->fid())) {
//                                    std::cout << "vid\t" << v_head << "\tdist\t" << dist[v_head][k] << "\tv_d\t" << v_d << "\tdelta_b\t" << delta_b << std::endl;
                                    continue;
                                }

                            }
                            else {
                                bool endForward = (dist[v_head][k] == 0);

                                if (dist[v].find(k) == dist[v].end())
                                    dist[v][k] = dist[v_head][k] + v_d;
                                else if (dist[v][k] > dist[v_head][k] + v_d) {
                                    dist[v][k] = dist[v_head][k] + v_d;
                                }
                                // Backtrack
                                
                                while(invert.find(v_head)!=invert.end()){
    //                                if (fragment->fid() == 1) VLOG(1) << "Invert\t " << invert.size();
                                    auto iParent = invert[v_head];
                                    if(dist[iParent].find(k) == dist[iParent].end())
                                        dist[iParent][k] = dist[v_head][k] - 1;
                                    else
                                        dist[iParent][k] = std::min(dist[iParent][k], dist[v_head][k] - 1);
                                    if(endForward) {
                                        if (fragment->fid() == 1) VLOG(1) << "Invert\t " << invert.size();
                                        k_forward_search.insert(iParent);
                                    }
                                    v_head = iParent;
                                }
                                 

                                if (endForward) {
                                    break;
//                                    if(FPrint(frag->fid())) std::cout << "<<<ENDPFKWS>>>" << std::endl;
                                }

                                if(delta_b > v_d) {
                                    if (fd[v_head].find(k) == fd[v_head].end())
                                        fd[v_head].insert(std::make_pair(k, delta_b - v_d));
                                    else if (fd[v_head][k] < delta_b - v_d) {
                                        fd[v_head][k] = delta_b - v_d;
                                    }
                                    active[v_head] = true;
//                                    if(FPrint(frag->fid())) std::cout << "<<<ENDPFKWS Got>>>\t" << v_head << "\tv_d\t" << delta_b-v_d << std::endl;
                                }
                            }
                        } else {
                            if(delta_b > v_d) {
                                if (fd[v_head].find(k) == fd[v_head].end())
                                    fd[v_head].insert(std::make_pair(k, delta_b - v_d));
                                else if (fd[v_head][k] < delta_b - v_d) {
                                    fd[v_head][k] = delta_b - v_d;
                                }
                                active[v_head] = true;
                            }
//                            std::cout << "delta_b\t" << delta_b << "\tv_d\t" << v_d << std::endl;

                            auto et = frag->_GetOutgoingEdges(v_head);
                            for (auto &iter : et) {
                                vid_t v_dst = iter.dst();
//                                if(FPrint(frag->fid())) std::cout << "v\t" << v << "PUSHING...\t" << frag->Gid2Oid(frag->Lid2Gid(v_dst)) << std::endl;
                                if (v_visited.find(v_dst) != v_visited.end()) continue;
                                double len = frag->GetData(iter).AsDouble();
                                v_heap.push(std::make_pair(-(v_d + len), v_dst));
//                            invert.insert(std::make_pair(v_dst,v));
                            }
                        }
                    }

                    if (dist[v].find(k) != dist[v].end())
                        ans_score[v] += dist[v][k];
                    else {
                        f_ans.insert(v);
                        continue;
                    }
                    if (dist[v].size() == keywords.size() &&
                        (ans_k.size() < topk || ans_score[v] < ans_k.top().first)) {
                        lower_bound = insert_ans(ans_k, topk, ans_score[v], v);
                        if(FPrint(frag->fid())) std::cout << frag->fid() << "\t PEval - FKWS - Lower bound\t" << lower_bound << "\t" << v << std::endl;
                        if(np) {
                            frag->notify(frag->fid(), lower_bound);
                            lower_bound = frag->push();
                        }
                    }
                }
                VLOG(1) << "Fid\t" << frag->fid() << "\tcnt skip\t" << cnt_skip;
            }
        }

        /**
        vid_t gvnum = frag->GetOuterVerticesNum() + ivnum;

        for (vid_t v = 0; v < gvnum - 2; v++){
            auto dsts = frag->OEDests(v);
            if(FPrint(frag->fid())) std::cout << ">>>>>>>>> For\t" << v << "\tgid\t" << frag->Gid2Oid(frag->Lid2Gid(v)) << "\tdst numbers\t" << std::endl;
            for(auto dst: dsts){
                if(FPrint(frag->fid())) std::cout << ">>>>>>>>>>>>>>>>>>>dst\t" << dst << std::endl; ;
            }
        }
         */




            // Init messages

        for (vid_t v = ivnum; v < gvnum; v++) {
            vid_t gvid = frag->Lid2Gid(v);
            if (active.find(v) != active.end()) {
                /** Forward search on other fratgments, MSG type is 2*/
                cMsg[v].insert(std::make_pair(2, fd[v]));

                int dfid = fragment->GetFragId(gvid);
                messages.SyncStateOnOuterVertex(dfid, VidMapDoublePairMsg(gvid, cMsg[v]));
//                if (FPrint(frag->fid())) std::cout << " MSG INC \t " << v << "\tfid\t" << dfid << std::endl;

            }

        }
        /**
        if (FPrint(frag->fid())) {
            std::cout << "PEval -- FKWS -- MSG # \t " << active.size() << std::endl;
            for(auto as: active){
                std::cout << "PEval -- FKWS -- MSG -- list\tv\t" <<as.first << "\toid\t" << frag->Gid2Oid(frag->Lid2Gid(as.first)) << std::endl;
            }
        }
         */


        frag->SetIntermediateData(std::to_string(0), Any(-1));
        frag->SetIntermediateData(std::to_string(2), Any(lower_bound));
//        VLOG(1) << fragment->fid() << "\tMSG END\t\t#\t";

    }
    void KWSPINE::PEval(unique_ptr<IFragment> &fragment, MessageBuffer &messages,
                       result_frag_t &partial_result,
                       shared_ptr<IUDContext> &context_ptr,
                       const Vector<Any> &query) {
        VLOG(1) << "PEval on PIE model";
    }

    bool refine_ans(vid_t u, std::unordered_map<vid_t, std::unordered_map<int, double>>& dist, std::unordered_map<vid_t, double>& ans_score, int k, double v2k, double max){
        if(v2k > max) {
            return false;
        }
        if (dist[u].find(k) == dist[u].end()){
            dist[u][k] = v2k;
            ans_score[u] += v2k;
            return true;
        } else if (dist[u][k] > v2k) {
            dist[u][k] = v2k;
            ans_score[u] -= v2k;
            return true;
        }
        return false;
    }
    void KWSPINE::PINE_IncEval(unique_ptr<IFragment> &fragment, MessageBuffer &messages,
                               result_frag_t &partial_result,
                               shared_ptr<IUDContext> &context_ptr,
                               const Vector<Any> &query){

        Graph pattern(query[0].AsGraph());
        Vector<int> keywords;

        for (vid_t u = 0; u < pattern.GetVerticesNum(); u++) {
            int tag = pattern.GetVertexData(u).ToInt32();

            keywords.push_back(tag);
        }

        KWSContext *kc = dynamic_cast<KWSContext *>(context_ptr.get());

        /** Accumulate the messages */
        VidMapDoublePairMsg msg;
//        auto &fd = kc->fd;
        auto &b_msg = kc->b_msg;
        auto &f_msg = kc->f_msg;
//        auto &invert_f_msg = kc->invert_f_msg;

        while (messages.GetMessage(msg)) {
            vid_t u = fragment->Gid2Lid(msg.vid());
            auto m = msg.value();
            for (auto k: keywords) {
                if (m.find(0) != m.end()){
                    std::unordered_map<int, double> msg_0 = m[0];
//                    if(FPrint(frag->fid())) std::cout << "MSGPRO-BKWS\t" << msg_0.size() << std::endl;
                    if(msg_0.find(k) != msg_0.end()){
                        if(b_msg.find(u) == b_msg.end() || b_msg[u].find(k) == b_msg[u].end() || b_msg[u][k] > msg_0[k])
                            b_msg[u][k] = msg_0[k];
                    }
                }
                if (m.find(1) != m.end()){
                    std::unordered_map<int, double> msg_1 = m[1];
                    if(msg_1.find(k) != msg_1.end()){
                        if(b_msg.find(u) == b_msg.end() || b_msg[u].find(k) == b_msg[u].end() || b_msg[u][k] > msg_1[k])
                            b_msg[u][k] = msg_1[k];
                    }
                }
                if (m.find(2) != m.end()) {
                    std::unordered_map<int, double> msg_2 = m[2];
                    if(msg_2.find(k) != msg_2.end()){
                        if(f_msg.find(u) == f_msg.end() || f_msg[u].find(k) == f_msg[u].end() || f_msg[u][k] < msg_2[k]) {
                            if(FPrint(fragment->fid())) std::cout << "MSGPRO-FKWS\t" << msg_2.size() << std::endl;
                            f_msg[u][k] = msg_2[k];
                        }
                    }
                }
            }
        }
        if(enable_pine) {
            bool BooF = true;
            if(fb_mode_ == INC_MSG_MODE)
                BooF = select_bkws_fkws(context_ptr, false);
            if(fb_mode_ == DES_MSG_MODE)
                BooF = select_bkws_fkws(context_ptr, true);
            if(fb_mode_ == FFirst_MODE)
                BooF = select_bkws_fkws_ffirst(context_ptr);
            if(fb_mode_ == BFirst_MODE)
                BooF = select_bkws_fkws_bfirst(context_ptr);
            if(fb_mode_ == Random_MODE)
                BooF = select_bkws_fkws_random(context_ptr);
            if(fb_mode_ == COST_MODE)
                BooF = select_bkws_fkws_by_cost(context_ptr, false);
            if(fb_mode_ == COST_REVERSE_MODE)
                BooF = select_bkws_fkws_by_cost(context_ptr, true);
            if (BooF) {
//                if (FPrint(fragment->fid()))
                    std::cout << "******************* ON PINE MODEL -- Stage IncEval -- BKWS *******************"
                              << std::endl;
                PINE_IncEval_BKWS(fragment, messages, partial_result, context_ptr, query);
            } else {
//                if (FPrint(fragment->fid()))
                    std::cout << "******************* ON PINE MODEL -- Stage IncEval -- FKWS *******************"
                              << std::endl;
                PINE_IncEval_FKWS(fragment, messages, partial_result, context_ptr, query);
            }
        } else {
            std::cout << "******************* ON PINE MODEL -- Stage IncEval -- BKWS *******************"
                      << std::endl;
            PINE_IncEval_BKWS(fragment, messages, partial_result, context_ptr, query);
            std::cout << "******************* ON PINE MODEL -- Stage IncEval -- FKWS *******************"
                      << std::endl;
            PINE_IncEval_FKWS(fragment, messages, partial_result, context_ptr, query);
        }
    }

    void KWSPINE::PINE_IncEval_BKWS(unique_ptr<IFragment> &fragment, MessageBuffer &messages,
                               result_frag_t &partial_result,
                               shared_ptr<IUDContext> &context_ptr,
                               const Vector<Any> &query){
        ImmutableEdgecutFragment *frag =
                dynamic_cast<ImmutableEdgecutFragment *>(fragment.get());

        KWSContext *kc = dynamic_cast<KWSContext *>(context_ptr.get());
        auto &dist = kc->dist;
        auto &active = kc->active;
        active = std::unordered_map<vid_t, bool>();
        auto &ans_score = kc->ans_score;
        auto &ans_k = kc->ans_k;
        auto &b_msg = kc->b_msg;
//        auto &f_msg = kc->f_msg;
        auto &heaps = kc->heaps;
        std::map<vid_t, std::unordered_map<unsigned, std::unordered_map<int, double>>> cMsg;

        double lower_bound;
        if(!np)
            lower_bound = frag->GetIntermediateData(std::to_string(2)).AsDouble();
        else
            lower_bound = frag->push();
        if(FPrint(frag->fid())) std::cout << "\t lower_bound init ....\t" << lower_bound << std::endl;

        Graph pattern(query[0].AsGraph());
        Vector<int> keywords;
        for (vid_t u = 0; u < pattern.GetVerticesNum(); u++) {
            int tag = pattern.GetVertexData(u).ToInt32();

            keywords.push_back(tag);
        }

        vid_t ivnum = frag->GetInnerVerticesNum();
        vid_t ovnum = frag->GetOuterVerticesNum();

//        std::map<int, std::priority_queue<std::pair<double, vid_t>>> heaps;
        std::unordered_map<int, std::vector<bool>> visiteds;

        // Init the priority queues
        for (auto k: keywords) {
            heaps[k] = std::priority_queue<std::pair<double, vid_t>>();
            visiteds[k] = std::vector<bool>(ivnum + ovnum, false);
        }


        std::unordered_map<int, double> heads = std::unordered_map<int, double>();
        std::unordered_map<int, int> cnts;


        for(auto b_m: b_msg){
            vid_t u = b_m.first;
            std::unordered_map<int, double> dis = b_m.second;
//            if(FPrint(frag->fid())) std::cout << u << "\t MSG \t" << dis.size() << std::endl;

            for (auto k: keywords) {
                // TODO: backward search

                /** 1) dis: incoming message 2) dist: value in current fragment*/
                /**
                for(auto item: dis)
                    if(FPrint(frag->fid())) std::cout << "MSG:\tk\t" << item.first << "\td\t" << item.second << "\tv(lid)\t" << u << "\tv(oid)\t" << frag->Gid2Oid(frag->Lid2Gid(u))<< std::endl;
                */
                if (dis.find(k) == dis.end())
                    continue;

                bool refined = refine_ans(u, dist, ans_score, k, dis[k], max);

                if (!refined) continue;

                active[u] = true;

                /** insert the adj vertices of out-Portal nodes*/
                auto es = frag->_GetIncomingEdges(u);
                for (auto &iter : es) {
//                    if(FPrint(frag->fid())) std::cout << k << "\t MSG_d \t" << dis[k] << std::endl;
                    vid_t v = iter.src();
                    double len = frag->GetData(iter).AsDouble();
                    if (dist[u][k] + len <= max) {
//                            if(FPrint(frag->fid())) std::cout << "Pushing >> \t id\t" << v << "\td\t" << (dist[u][k] + len) << "\tk\t" << k << std::endl;
                        heaps[k].push(std::make_pair(-(dist[u][k] + len), v));
                    }
                }
                if (u < ivnum && dist[u].size() == keywords.size() &&
                    ((!ans_k.empty() && ans_k.top().first > ans_score[u]) || ans_k.size() < topk)) {
                    lower_bound = insert_ans(ans_k, topk, ans_score[u], u);
                    if (FPrint(frag->fid()))
                        std::cout << frag->fid() << "\tInc - Lower bound\t" << lower_bound << "\t" << u << std::endl;

                    if (np)
                        frag->notify(frag->fid(), lower_bound);
                }
            }

        }
        b_msg.clear();

        double sum_heads = 0;

        for(auto k: keywords){
            if(!heaps[k].empty())
                heads[k] = - heaps[k].top().first;
            sum_heads += heads[k];
        }

        if (FPrint(frag->fid()))
            std::cout << "fid\t" << frag->fid() << "\tSum of heads\t" << sum_heads << "\t lower_bound\t" << lower_bound << std::endl;
        // Backward ....................

        while(!heaps.empty()){
//        while(sum_heads < lower_bound && !heaps.empty()){
//            if(FPrint(frag->fid())) std::cout << "Sum of heads\t" << sum_heads << "\t lower_bound\t" << lower_bound << std::endl;
            int k = select_min_head(heads, cnts);
//            if(FPrint(frag->fid()))  std::cout << "select keywords\t" << k << std::endl;
            if (k == -1) break;
            auto& heap = heaps[k];
            auto& visited = visiteds[k];

            if (!heap.empty()) {
                vid_t u = heap.top().second;
                double d = -heap.top().first;

                heap.pop();
                if(visited[u]) continue;

                cnts[k] ++;
                if (d > heads[k]){
                    sum_heads += (d - heads[k]);
                    heads[k] = d;
                }

                visited[u] = true;

                if (d >= max)
                    break;


                /** Check if u is a root */

                bool refined = refine_ans(u, dist, ans_score, k, d, max);


//                if(FPrint(frag->fid()) && refined) std::cout << "Refined \t" << u << "\t" << std::endl;

                if(!refined) continue;
                active[u] = true;
                if (u < ivnum && dist[u].size() == keywords.size() &&
                    ((!ans_k.empty() && ans_k.top().first > ans_score[u]) || ans_k.size() < topk)) {
                    lower_bound = insert_ans(ans_k, topk, ans_score[u], u);
                    if(FPrint(frag->fid())) std::cout << frag->fid() << "\t Inc - Lower bound\t" << lower_bound << "\t" << u << std::endl;
                    if (np)
                        frag->notify(frag->fid(), lower_bound);
                }

                auto es = frag->_GetIncomingEdges(u);
                for (auto &iter : es) {
                    vid_t v = iter.src();
                    if (visited[v]) continue;
                    double len = frag->GetData(iter).AsDouble();
                    heap.push(std::make_pair(-(d + len), v));
                }
            } else {
                heaps.erase(k);
                heads[k] = std::numeric_limits<double>::max();
            }

        }
        /**
        if(FPrint(frag->fid())){
            for(unsigned i = 0; i< ivnum; i++){
                std::cout << "v(lid)\t" << i << "\tv(gid)\t" << frag->Gid2Oid(frag->Lid2Gid(i)) << "\t"<< std::endl;
            }
        }
         */
        if (fragment->fid() == 1) VLOG(1) << "Size of active\t " << active.size();
        // Init messages
//        int cnt_msg = 0;
//        bool flag = false;
//        if (!f_msg.empty()) flag = false;
        for (vid_t v = ivnum; v < ivnum + ovnum; v++) {
            vid_t gvid = frag->Lid2Gid(v);
            cMsg[v].insert(std::make_pair(0, dist[v]));
            if (active.find(v) != active.end()) {
                int dfid = fragment->GetFragId(gvid);
                messages.SyncStateOnOuterVertex(dfid, VidMapDoublePairMsg(gvid, cMsg[v]));
//                cnt_msg ++;
                /** Backward search on other fratgments, MSG type is 0

                if (frag->IsIncomingBorderVertex(v))
                    messages.SendMsgThroughIEdges(frag, v,
                                                  VidMapDoublePairMsg(gvid, cMsg[v]));
*/
                /** Continue the forward expansion on other fratgments, MSG type is 1 */
            }

            /** Forward search is not finished
            if(flag){
                if (frag->IsOutgoingBorderVertex(v)){
                    messages.SendMsgThroughIEdges(frag, v,
                                                  VidMapDoublePairMsg(gvid, cMsg[v]));
                }
                flag = false;
            }
             */
        }
        if(FPrint(frag->fid())) std::cout << "# of Msg \t" << active.size() << std::endl;
        frag->SetIntermediateData(std::to_string(0), Any(-1));
        frag->SetIntermediateData(std::to_string(2), Any(lower_bound));
        LOG(INFO) << "# of answers \t" << ans_k.size();
    }

    void KWSPINE::PINE_IncEval_FKWS(unique_ptr<IFragment> &fragment, MessageBuffer &messages,
                               result_frag_t &partial_result,
                               shared_ptr<IUDContext> &context_ptr,
                               const Vector<Any> &query){
        ImmutableEdgecutFragment *frag =
                dynamic_cast<ImmutableEdgecutFragment *>(fragment.get());

        KWSContext *kc = dynamic_cast<KWSContext *>(context_ptr.get());
        auto &dist = kc->dist;
        auto &active = kc->active;
        active = std::unordered_map<vid_t, bool>();
        auto &ans_score = kc->ans_score;
        auto &ans_k = kc->ans_k;
        std::map<vid_t, std::unordered_map<unsigned, std::unordered_map<int, double>>> cMsg;
        auto &fd = kc->fd;
        auto &f_msg = kc->f_msg;
//        auto &b_msg = kc->b_msg;
        auto &visited_forward = kc->visited_forward;
        auto &heaps = kc->heaps;
        /** TODO: configable NP-model*/
//        auto lower_bound = readBound();
        double lower_bound;
        if(!np)
            lower_bound = frag->GetIntermediateData(std::to_string(2)).AsDouble();
        else
            lower_bound = frag->pull();
        Graph pattern(query[0].AsGraph());
        Vector<int> keywords;
        for (vid_t u = 0; u < pattern.GetVerticesNum(); u++) {
            int tag = pattern.GetVertexData(u).ToInt32();
            keywords.push_back(tag);
        }

        VidMapDoublePairMsg msg;
        if(FPrint(frag->fid()))
            std::cout << "size of fd\t" << fd.size() << std::endl;

        for(auto &f_m: f_msg) {
            vid_t u = f_m.first;
            std::unordered_map<int, double>& fd_u = f_m.second;
            for (auto k:keywords) {
                if (fd_u[k] > fd[u][k]) {
                    fd[u][k] = fd_u[k];
                    if (visited_forward.find(u) != visited_forward.end())
                        visited_forward.erase(u);
                }
            }
        }
        f_msg.clear();
//        std::cout << "Size after clearing\t" << f_msg.size() << std::endl;

        vid_t ivnum = frag->GetInnerVerticesNum();

//        std::map<int, std::priority_queue<std::pair<double, vid_t>>> heaps;
        // Forwarding..................


        if (fragment->fid() == 5) VLOG(1) << "Forwarding..........";
        for (auto k: keywords) {
            heaps[k] = std::priority_queue<std::pair<double, vid_t>>();
        }

        for (vid_t v = 0; v < ivnum; v++) {
            if (dist[v].size() == keywords.size() || dist[v].size() == 0) {
                continue;
            }
            if (lower_bound != std::numeric_limits<double>::max() && lower_bound <= ans_score[v]) {
                continue;
            }

            for (auto k: keywords) {
                if (dist[v].find(k) == dist[v].end()) {
                    if (lower_bound != std::numeric_limits<double>::max()) {
                        if (lower_bound - ans_score[v] < max)
                            heaps[k].push(std::make_pair(-(lower_bound - ans_score[v]), v));
                        else
                            heaps[k].push(std::make_pair(-max, v));
                    } else
                        heaps[k].push(std::make_pair(-max, v));
                }
            }
        }

        /** FIXME: Above bugs identified*/
        // f_ans records the answers missing some keyword (f_ans = false answers)
        std::unordered_set<vid_t> f_ans;

        if(FPrint(frag->fid()))
            std::cout << "Size of active before forward\t " << active.size() << std::endl;

        for (auto k: keywords) {
            if (fragment->fid() == 5) VLOG(1) << "Forwarding1\t" << k << "\t#\t" << heaps[k].size();

            // Those have been forwardly search
            while (!heaps[k].empty()) {
                vid_t v = heaps[k].top().second;
                double delta_b = -heaps[k].top().first;
                heaps[k].pop();
                if (visited_forward.find(v) != visited_forward.end())
                    continue;

                if (delta_b > lower_bound - ans_score[v]) delta_b = lower_bound - ans_score[v];

                if (f_ans.find(v) != f_ans.end()) {
                    continue;
                }
                std::priority_queue<std::pair<double, vid_t>> v_heap = std::priority_queue<std::pair<double, vid_t>>();
                v_heap.push(std::make_pair(0, v));

//                std::unordered_map<vid_t, vid_t> invert;

                std::unordered_set<vid_t> v_visited;
                while (!v_heap.empty()) {
                    vid_t v_head = v_heap.top().second;
                    v_visited.insert(v_head);
                    double v_d = -v_heap.top().first;
                    v_heap.pop();

                    if (v_d >= delta_b || (dist[v].find(k) != dist[v].end() && v_d >= dist[v][k]))
                        break;

                    bool hasK = (dist[v_head].find(k) != dist[v_head].end());
                    if (hasK) {
                        if (dist[v_head][k] + v_d > delta_b)
                            continue;
                        else {
                            bool endForward = (dist[v_head][k] == 0);

                            if (dist[v].find(k) == dist[v].end())
                                dist[v][k] = dist[v_head][k] + v_d;
                            else if (dist[v][k] > dist[v_head][k] + v_d) {
                                dist[v][k] = dist[v_head][k] + v_d;
                            }

                            // Backtrack
                            /*
                            while(invert.find(v_head)!=invert.end()){
//                                if (fragment->fid() == 1) VLOG(1) << "Invert\t " << invert.size();
                                auto iParent = invert[v_head];
                                if(dist[iParent].find(k) == dist[iParent].end())
                                    dist[iParent][k] = dist[v_head][k] - 1;
                                else
                                    dist[iParent][k] = std::min(dist[iParent][k], dist[v_head][k] - 1);
                                if(endForward) {
                                    if (fragment->fid() == 1) VLOG(1) << "Invert\t " << invert.size();
                                    k_forward_search.insert(iParent);
                                }
                                v_head = iParent;
                            }
                             */

                            if (endForward)
                                break;
//                            if (frag->IsOutgoingBorderVertex(v_head)) {
                            if(delta_b > v_d) {
                                if (fd[v_head].find(k) == fd[v_head].end()) {
                                    fd[v_head].insert(std::make_pair(k, delta_b - v_d));
                                    active[v_head] = true;
                                } else if (fd[v_head][k] < delta_b - v_d) {
                                    fd[v_head][k] = delta_b - v_d;
                                    active[v_head] = true;
                                }
//                                if(FPrint(frag->fid()))
//                                    std::cout << "k\t" << k << "\tdelta_b\t" << delta_b << "\tv_d\t" << v_d << "\tv_head\t" << v_head << "\tfd_dist\t" <<fd[v_head][k] << std::endl;

                            }
//                            }
                        }
                    } else {
//                        if (frag->IsOutgoingBorderVertex(v_head)) {
                            if(delta_b > v_d) {
                                if (fd[v_head].find(k) == fd[v_head].end()) {
//                                    if(FPrint(frag->fid())) std::cout << "3rdC k\t" << k << "\tdelta_b\t" << delta_b << "\tv_d\t" << v_d  << "\tv_head\t" << v_head << "\tfd_dist\t" <<fd[v_head][k] << std::endl;
                                    fd[v_head].insert(std::make_pair(k, delta_b - v_d));
                                    active[v_head] = true;
                                } else if (fd[v_head][k] < delta_b - v_d) {
//                                    if(FPrint(frag->fid())) std::cout << "4thC k\t" << k << "\tdelta_b\t" << delta_b << "\tv_d\t" << v_d  << "\tv_head\t" << v_head << "\tfd_dist\t" <<fd[v_head][k] << std::endl;
                                    fd[v_head][k] = delta_b - v_d;
                                    active[v_head] = true;
                                }
//                                if(FPrint(frag->fid()))
//                                    std::cout << "k\t" << k << "delta_b\t" << delta_b << "\tv_d\t" << v_d << "\tv_head\t" << v_head  << "\tfd_dist\t" <<fd[v_head][k] << std::endl;

                            }

//                        }
                        auto et = frag->_GetOutgoingEdges(v_head);
                        for (auto &iter : et) {
                            vid_t v_dst = iter.dst();
                            if (v_visited.find(v_dst) != v_visited.end()) continue;
                            double len = frag->GetData(iter).AsDouble();
                            v_heap.push(std::make_pair(-(v_d + len), v_dst));
//                            invert.insert(std::make_pair(v_dst,v));
                        }
                    }
                }

                if (dist[v].find(k) != dist[v].end())
                    ans_score[v] += dist[v][k];
                else {
                    f_ans.insert(v);
                    continue;
                }
                if (dist[v].size() == keywords.size() && (ans_k.size() < topk || ans_score[v] < ans_k.top().first)) {
                    lower_bound = insert_ans(ans_k, topk, ans_score[v], v);
                }
            }
        }
        if(FPrint(frag->fid()))
            std::cout << "After forward search\t" << fd[590][2] << "\t fd \t" << active.size() << std::endl;

//        bool flag = false;
//        if(!b_msg.empty()) flag = true;
        // Init messages

        auto ovnum = frag->GetOuterVerticesNum();
        for (vid_t v = ivnum; v < ivnum+ovnum; v++) {
            vid_t gvid = frag->Lid2Gid(v);
            if (active.find(v) != active.end()) {
                cMsg[v].insert(std::make_pair(2, fd[v]));
                cMsg[v].insert(std::make_pair(1, dist[v]));
                int dfid = fragment->GetFragId(gvid);
                messages.SyncStateOnOuterVertex(dfid, VidMapDoublePairMsg(gvid, cMsg[v]));
            }
            /**
            if(flag) {
                int dfid = fragment->GetFragId(gvid);
                messages.SyncStateOnOuterVertex(dfid, VidMapDoublePairMsg(gvid, cMsg[v]));
                flag = false;
            }
             */
        }
        frag->SetIntermediateData(std::to_string(2), Any(lower_bound));
        LOG(INFO) << "# of answers \t" << ans_k.size();

    }

    void KWSPINE::IncEval(unique_ptr<IFragment> &fragment, MessageBuffer &messages,
                         result_frag_t &partial_result,
                         shared_ptr<IUDContext> &context_ptr,
                         const Vector<Any> &query) {

        LOG(INFO) << "For soundness";
    }

    void KWSPINE::ProcessMsgInCoordinator(const Vector<Vector<Map<String, Any>>> &messages,
                                         const Vector<Any> &query, Map<String, Any> &message_to_worker,
                                         bool &is_terminate) {
        double lower_bound = std::numeric_limits<double>::max();

        for (auto wMsg: messages) {
            for (auto wsMsg: wMsg) {
                lower_bound = std::min(lower_bound, wsMsg[std::to_string(2)].AsDouble());
            }
        }

        message_to_worker[std::to_string(2)] = Any(lower_bound);
        message_to_worker[std::to_string(1)] = Any(0);
        message_to_worker[std::to_string(0)] = Any(-1);
    }

    void KWSPINE::Assemble(result_frag_t &partial_result, result_frag_t &result,
                          const Vector<Any> &query) {
    }

    void KWSPINE::WriteToFile(unique_ptr<IFragment> &fragment,
                             result_frag_t &partial_result,
                             shared_ptr<IUDContext> &context_ptr,
                             const String &prefix, const Vector<Any> &query) {
    }
}  // namespace grape
