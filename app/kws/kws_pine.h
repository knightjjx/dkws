#ifndef APP_KWS_KWSPINE_H_
#define APP_KWS_KWSPINE_H_

#include <memory>

#include "grape/any.h"
#include "grape/app/i_app.h"
#include "grape/config.h"



namespace grape {

class KWSPINE : public IApp {
 public:
  KWSPINE() {}
  ~KWSPINE() {}

  void Init(unique_ptr<IFragment> &fragment, result_frag_t &partial_result,
            shared_ptr<IUDContext> &context_ptr, const Vector<Any> &query);
  void PINE_PEval(unique_ptr<IFragment> &fragment, MessageBuffer &messages,
          result_frag_t &partial_result, shared_ptr<IUDContext> &context_ptr,
               const Vector<Any> &query);
  void PINE_PEval_BKWS(unique_ptr<IFragment> &fragment, MessageBuffer &messages,
          result_frag_t &partial_result, shared_ptr<IUDContext> &context_ptr,
                    const Vector<Any> &query);

  void PINE_PEval_FKWS(unique_ptr<IFragment> &fragment, MessageBuffer &messages,
          result_frag_t &partial_result, shared_ptr<IUDContext> &context_ptr,
          const Vector<Any> &query);


    void PEval(unique_ptr<IFragment> &fragment, MessageBuffer &messages,
             result_frag_t &partial_result, shared_ptr<IUDContext> &context_ptr,
             const Vector<Any> &query);

  void PINE_IncEval(unique_ptr<IFragment> &fragment, MessageBuffer &messages,
                 result_frag_t &partial_result,
                 shared_ptr<IUDContext> &context_ptr, const Vector<Any> &query);

    void PINE_IncEval_BKWS(unique_ptr<IFragment> &fragment, MessageBuffer &messages,
                      result_frag_t &partial_result,
                      shared_ptr<IUDContext> &context_ptr, const Vector<Any> &query);
    void PINE_IncEval_FKWS(unique_ptr<IFragment> &fragment, MessageBuffer &messages,
                      result_frag_t &partial_result,
                      shared_ptr<IUDContext> &context_ptr, const Vector<Any> &query);

  void IncEval(unique_ptr<IFragment> &fragment, MessageBuffer &messages,
               result_frag_t &partial_result,
               shared_ptr<IUDContext> &context_ptr, const Vector<Any> &query);

  void Assemble(result_frag_t &partial_result, result_frag_t &result,
                const Vector<Any> &query);

  void WriteToFile(unique_ptr<IFragment> &fragment,
                   result_frag_t &partial_result,
                   shared_ptr<IUDContext> &context_ptr, const String &prefix,
                   const Vector<Any> &query);

  void ProcessMsgInCoordinator(const Vector<Vector<Map<String, Any>>>& messages,
                                 const Vector<Any>& query,
                                 Map<String, Any>& message_to_worker,
                                 bool& is_terminate);
};

extern "C" IApp *Create() {
  VLOG(1) << "Create a instance of KWSPINE.";
  return new KWSPINE;
}
extern "C" void Destroy(IApp *p) {
  VLOG(1) << "Destroy a instance of KWSPINE.";
  delete p;
}

}  // namespace grape

#endif  // APP_KWS_KWSPINE_H_
